package com.sberbank.testtask.counterservice.rest;

import com.sberbank.testtask.counterservice.storage.CounterStorage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * REST Controller containing mapped requests to manipulate in-memory counter storage
 */
@RestController
public class CounterController {

    // *************************
    // Single counter operations
    // *************************

    /**
     * Get value of specified counter
     *
     * @param key - name of specified counter (= key of {@link CounterStorage})
     * @return {@link ResponseEntity<CounterRestInfo>} with given key, value of specified counter (if it exists);
     * if it doesn't exist - with error description
     */
    @GetMapping("/counters/single/{key}")
    public ResponseEntity<CounterRestInfo> getCounter(@PathVariable("key") String key) {

        final Long counterValue = CounterStorage.counterMap.get(key);

        HttpStatus status = null;
        final CounterRestInfo response = new CounterRestInfo().setName(key).setValue(counterValue);

        if (counterValue != null) {
            status = HttpStatus.OK;
        } else {
            response.setAdditionalInfo(String.format("No counter with name \"%s\"", key));
            status = HttpStatus.BAD_REQUEST;
        }

        return new ResponseEntity<>(response, status);
    }

    /**
     * Create new counter with specified name
     *
     * @param key - name of specified counter (= key of {@link CounterStorage})
     * @return {@link ResponseEntity<CounterRestInfo>} with given key, initial value (0) of specified counter
     * (if it was created in current call); if specified counter already exists - with error description
     */
    @PostMapping("/counters/single/{key}")
    public ResponseEntity<CounterRestInfo> createCounter(@PathVariable("key") String key) {

        final Long counterValue = CounterStorage.counterMap.putIfAbsent(key, 0L);

        HttpStatus status = null;
        final CounterRestInfo response = new CounterRestInfo().setName(key);

        if (counterValue != null) {
            status = HttpStatus.BAD_REQUEST;
            response.setAdditionalInfo(String.format("Counter with name \"%s\" already exists", key));
        } else {
            status = HttpStatus.OK;
            response.setValue(0L);
        }

        return new ResponseEntity<>(response, status);
    }

    /**
     * Increment existing counter with specified name
     *
     * @param key - name of specified counter (= key of {@link CounterStorage})
     * @return {@link ResponseEntity<CounterRestInfo>} with given key, current value of specified counter
     * (if it exists); if specified counter doesn't exist - with error description
     */
    @PutMapping("/counters/single/{key}")
    public ResponseEntity<CounterRestInfo> incrementCounter(@PathVariable("key") String key) {

        final Long counterValue = CounterStorage.counterMap.computeIfPresent(key, (k, counter) -> counter += 1);

        HttpStatus status = null;
        final CounterRestInfo response = new CounterRestInfo().setName(key);

        if (counterValue == null) {
            status = HttpStatus.BAD_REQUEST;
            response.setAdditionalInfo(String.format("Counter with name \"%s\" does not exist", key));
        } else {
            status = HttpStatus.OK;
            response.setValue(counterValue);
        }

        return new ResponseEntity<>(response, status);
    }

    /**
     * Delete existing counter with specified name
     *
     * @param key - name of specified counter (= key of {@link CounterStorage})
     * @return {@link ResponseEntity<CounterRestInfo>} with given key;
     * if specified counter already exists - with error description
     */
    @DeleteMapping("/counters/single/{key}")
    public ResponseEntity<CounterRestInfo> deleteCounter(@PathVariable("key") String key) {

        final Long removedValue = CounterStorage.counterMap.remove(key);

        HttpStatus status = null;
        final CounterRestInfo response = new CounterRestInfo().setName(key);

        if (removedValue == null) {
            status = HttpStatus.BAD_REQUEST;
            response.setAdditionalInfo(String.format("Counter with name \"%s\" does not exist", key));
        } else {
            status = HttpStatus.OK;
        }

        return new ResponseEntity<>(response, status);
    }

    // ***************************
    // Summary counters operations
    // ***************************

    /**
     * Get sum of all existing counters
     *
     * @return {@link Long} sum of all existing counters (or 0 if no counters exist)
     */
    @GetMapping("/counters/sum")
    public Long getCountersSum() {

        return CounterStorage.counterMap.values().stream().reduce(0L, Long::sum);
    }

    /**
     * Get names of all existing counters
     *
     * @return {@link Set<String>} of all existing names (or empty [] if no counters exist)
     */
    @GetMapping("/counters/names")
    public Set<String> getAllCounters() {

        return CounterStorage.counterMap.keySet();
    }
}
