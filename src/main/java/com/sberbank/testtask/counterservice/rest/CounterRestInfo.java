package com.sberbank.testtask.counterservice.rest;

public class CounterRestInfo {

    /**
     * Counter name (given from REST request)
     */
    private String name;

    /**
     * Value of specified counter (may be null if any error occured)
     */
    private Long value;

    /**
     * Error or warning description (may be empty)
     */
    private String additionalInfo;

    public CounterRestInfo() {
    }

    public String getName() {
        return name;
    }

    public CounterRestInfo setName(String name) {
        this.name = name;
        return this;
    }

    public Long getValue() {
        return value;
    }

    public CounterRestInfo setValue(Long value) {
        this.value = value;
        return this;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public CounterRestInfo setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
        return this;
    }
}
