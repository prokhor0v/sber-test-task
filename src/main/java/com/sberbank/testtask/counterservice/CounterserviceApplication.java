package com.sberbank.testtask.counterservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CounterserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CounterserviceApplication.class, args);
	}

}

/*
Описание задачи
Необходимо реализовать spring-boot приложение на JDK 8+, счетчик.

Требования
Посредством RestApi должна быть возможность:

Создать счетчик с уникальным именем;
Инкремент значения счетчика с указанным именем;
Получить значения счетчика с указанным именем;
Удалить счетчик с указанным именем;
Получить суммарное значение всех счетчиков;
Получить уникальные имена счетчиков в виде списка.
Значения всех счетчиков должны храниться в памяти приложения.
 */