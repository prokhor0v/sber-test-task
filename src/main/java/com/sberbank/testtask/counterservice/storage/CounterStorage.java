package com.sberbank.testtask.counterservice.storage;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CounterStorage {

    /**
     * key - counter name (unique)
     * value - counter value
     */
    public static final Map<String, Long> counterMap = new ConcurrentHashMap<>();

    /**
     * We don't need to create objects, so we close default constructor
     */
    private CounterStorage() {
        // no-ops
    }
}
